var raster = new ol.layer.Tile({
  source: new ol.source.MapQuest({layer: 'sat'})
});

var vector = new ol.layer.Vector({
  source: new ol.source.Vector({
    format: new ol.format.GeoJSON(),
    url: 'data/geojson/countries.geojson'
  }),
  extent: [-124.74, 24.96, -66.96, 49.38]
});

var map = new ol.Map({
  layers: [raster, vector],
  target: 'map2d',
  view: new ol.View({
    center: [0, 0],
    zoom: 2
  })
});


var ol3d = new olcs.OLCesium({map: map});
var scene = ol3d.getCesiumScene();

setTimeout(function(){
  ol3d.setEnabled(true);
  //ol3d.enableAutoRenderLoop();
},100);



var selectionStyle = new ol.style.Style({
  fill: new ol.style.Fill({
    color: [255, 255, 255, 0.6]
  }),
  stroke: new ol.style.Stroke({
    color: [0, 153, 255, 1],
    width: 3
  })
});

var selectedFeature;
// map.on('click', function(e) {
//   if (selectedFeature) {
//     selectedFeature.setStyle(null);
//   }
//   selectedFeature = map.forEachFeatureAtPixel(
//       e.pixel,
//       function(feature, layer) {
//         return feature;
//       });
//   if (selectedFeature) {
//     selectedFeature.setStyle(selectionStyle);
//   }
// });




// Show off 3D feature picking
var handler = new Cesium.ScreenSpaceEventHandler(scene.canvas);
var lastPicked;
handler.setInputAction(function(movement) {
  ol3d.getCamera().readFromView();
  var pickedObjects = scene.drillPick(movement.position);
   if (Cesium.defined(pickedObjects)) {
    for (i = 0; i < pickedObjects.length; ++i) {
      var picked = pickedObjects[i].primitive;
      if (picked.olFeature == lastPicked) continue;
      var feature = picked.olFeature;
      console.log('Picked feature', picked.olFeature);
      lastPicked = picked.olFeature;
      var center = ol.extent.getCenter(picked.olFeature.getGeometry().getExtent());
      //
      var pan = ol.animation.pan({
            duration: 2000,
            source: /** @type {ol.Coordinate} */ (map.getView().getCenter())
          });
          map.beforeRender(pan);
      map.getView().setCenter(center);
    }
  } else {
    lastPicked = undefined;
  }
}, Cesium.ScreenSpaceEventType.LEFT_CLICK);
